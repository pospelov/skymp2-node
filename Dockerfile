FROM node:10.12.0-alpine
RUN ln -fs /usr/share/zoneinfo/Europe/Moscow /etc/localtime
COPY package*.json ./
RUN npm i
COPY . .
RUN apk update && apk add git
RUN git clone https://gitlab.com/pospelov/skymp2-plugins.git plugins
ENTRYPOINT ["npm"]
CMD ["run", "start"]
