const fs = require('fs-extra');
const path = require('path');
const crypto = require('crypto');

const mongoose = require('mongoose');
const Koa = require('koa');
const cors = require('@koa/cors');
const bodyParser = require('koa-bodyparser');
const serve = require('koa-static');
const logger = require('koa-logger');
const passport = require('koa-passport');

const load = require('./src/espm/load');
const Invite = require('./src/invite/model');

let cfg;
try {
  cfg = require('./config');
}
catch (err) {
  cfg = require('./default-config');
  fs.writeFileSync('config.js', fs.readFileSync('default-config.js'));
}

const records = require('./src/records');

mongoose.connect(cfg.dataBaseUrl, { useNewUrlParser: true, poolSize: cfg.poolSize });
mongoose.Promise = Promise;
mongoose.set('useCreateIndex', true); // https://github.com/Automattic/mongoose/issues/6890
mongoose.set('useFindAndModify', false); // https://github.com/Automattic/mongoose/issues/5616
mongoose.set('debug', false);
let db = mongoose.connection;
db.on('error', error => {
  console.error(error.toString());
});
db.once('open', async () => {
  console.log('mongoose open');

  console.log(`created ${(await Invite.reserve(1000)).length} new invites`);
  fs.writeFileSync('invites.txt', (await Invite.find({})).map(x => x.text).filter(x => x[0] != parseInt(x[0])));

  if (process.env.RUN_CELLHOST) {
    console.log('cellhost');
    require('./src/cellhost');
    return;
  }

  console.log('loading game');
  let was = Date.now();
  let merged = load('plugins', ['Skyrim.esm', 'Update.esm', 'Dragonborn.esm']);
  console.log('loaded in', Date.now() - was, 'ms');

  let app = new Koa;
  let router = require('./src/router');
  app.context.mergedMaster = merged;
  app.use(cors());
  app.use(logger());
  app.use(bodyParser());
  app.use(passport.initialize());
  app.use(router.routes());
  app.use(router.allowedMethods());
  app.listen(cfg.port);
});
