const Router = require('koa-router');
const mongoose = require('mongoose');
const { MPCH } = require('../records');
const utils = require('../utils');

let router = new Router;

function formatSession(x) {
  return {
    date: x.lastUpdate,
    onlineDuration: Math.ceil(0.001 * (x.lastUpdate - x.lastLogin)),
    version: x.version,
    isNormalExit: x.isNormalExit
  };
}

function format(x) {
  let sessions = x.crashes.map(formatSession);
  return {
    displayName: x.displayName,
    totalOnlineDuration: Math.ceil(x.secondsPlayed + 0.001 * (x.lastUpdate - x.lastLogin)),
    online: x.lastUpdate >= Date.now() - MPCH.getTimeoutMs(),
    onlineDuration: Math.ceil(0.001 * (x.lastUpdate - x.lastLogin)),
    crashes: sessions.filter(y => !y.isNormalExit),
    sessions: sessions
  };
}

router.get('/users', async (ctx) => {
  ctx.body = (await MPCH.find({})).map(format);
});

router.get('/', async (ctx) => {
  ctx.body = {
    online: (await MPCH.findOnline()).length
  };
});

router.get('/users/:displayName', async (ctx) => {
  ctx.body = format(await MPCH.findOne({ displayName: ctx.params.displayName }));
});

function parseExchangeContent(str) {
  let tokens = str.split(' ');
  let myName = tokens[0];
  let partnerName = tokens[1];
  let myLock = tokens[2];
  let partnerLock = tokens[3];
  let myExchangeInv = [];
  let partnerExchangeInv = null;
  for (let i = 4; i < tokens.length; ++i) {
    if (tokens[i] === 'END_MY_INV') {
      partnerExchangeInv = [];
      continue;
    }
    let inf = tokens[i].split('=')[0];
    let count = tokens[i].split('=')[1];
    let espm = inf.split(':')[0];
    let id = parseInt(inf.split(':')[1], 16)
    if (partnerExchangeInv) partnerExchangeInv.push({ form: {id, espm}, count });
    else myExchangeInv.push({ form: {id, espm}, count });
  }
  partnerExchangeInv.pop();
  return { myName, partnerName, myLock, partnerLock, myExchangeInv, partnerExchangeInv };
}

router.post('/scriptevent', async (ctx) => {
  let { token, content, name } = ctx.request.body;

  await utils.sleep(500); // чтобы не давить на базу данных. клиенты флудят этими запросами

  if (name === 'exchange') {
    let mpch = await MPCH.findOne({ token });
    if (!mpch) return ctx.throw(404);

    content = parseExchangeContent(content);
    let updRes = await MPCH.updateOne({ token }, { exchangeEvent: content });
    //if (!updRes.nModified) return ctx.throw(403);

    let target = await MPCH.findOne({
      'exchangeEvent.partnerName': content.myName,
      'exchangeEvent.myName': content.partnerName,
      'exchangeEvent.myExchangeInv': content.partnerExchangeInv,
      'exchangeEvent.partnerExchangeInv': content.myExchangeInv
    });
    console.log('content.myExchangeInv:', content.myExchangeInv);
    console.log('content.partnerExchangeInv:', content.partnerExchangeInv);
    console.log('target:', !!target);
    if (target) {
      console.log(content.myExchangeInv, content.partnerExchangeInv)
      console.log(target.displayName)
      let me = { displayName: mpch.displayName };
      let he = { displayName: target.displayName };
      await MPCH.removeItem(he, content.partnerExchangeInv);
      try {
        await MPCH.removeItem(me, content.myExchangeInv);
      }
      catch (err) {
        await MPCH.addItem(he, content.partnerExchangeInv);
        throw err;
      }
      await MPCH.addItem(me, content.partnerExchangeInv);
      await MPCH.addItem(he, content.myExchangeInv);
      ctx.status = 204;
    }
    else ctx.status = 403;
  }
  else if (name === 'setvar') {
    let data = {};
    let varName = content.split('=')[0];
    let value = content.split('=')[1];
    data['scriptVars.' + varName] = value;
    let updRes = await MPCH.updateOne({ token }, data);
    ctx.status = 204;
    console.log(varName, '=', value, '(nModified is ' + updRes.nModified + ')');
  }
  else if (name === 'getvar') {
    let targetName = content.split('.')[0];
    let varName = content.split('.')[1];
    let target = await MPCH.findOne({ displayName: targetName });
    let was = Date.now();
    await MPCH.findOne({ token: target.token });
    console.log(Date.now() - was);
    if (target) {
      ctx.body = target.scriptVars[varName];
    }
    else {
      ctx.status = 404;
    }
  }
  else if (name === 'sendto') {
    let mpch = await MPCH.findOne({ token });
    if (!mpch) return ctx.throw(404);

    let targetName = content.split(' ')[0];
    let header = content.split(' ')[1];
    let content1 = content.split(' ').slice(2).join(' ');
    let updRes = await MPCH.updateOne({ displayName: targetName }, { $push: { scriptMessages: { from: mpch.displayName, header, content: content1 } } });
    if (updRes.nModified === 0) return ctx.throw(403);
    ctx.status = 204;
  }
  else if (name === 'nextmsg') {
    let mpch = await MPCH.findOne({ token });
    if (!mpch) return ctx.throw(404);
    let updRes = await MPCH.updateOne({ token }, { $pop: { scriptMessages: 1 } });
    if (updRes.nModified === 0) return ctx.throw(404);
    let msg = mpch.scriptMessages.pop();
    ctx.body = msg.from + ' ' + msg.header + ' ' + msg.content;
  }
});

module.exports = router;
