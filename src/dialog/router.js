const Router = require('koa-router');
const mongoose = require('mongoose');
const { MPCH } = require('../records');

let router = new Router;

const BTNID_ACCEPT_PARTY_INVITE = 'v20.hardcoded.acceptPartyInvite';
const BTNID_CLOSE_DIALOG = 'v20.hardcoded.closeDialog';

router.post('/:token/:btnid', async (ctx) => {
  let { btnid, token } = ctx.params;
  let mpch = await MPCH.findOne({ token });
  if (!mpch) return ctx.throw(404, 'MPCH not found');
  ctx.body = { newDialog: null };

  if (btnid === BTNID_CLOSE_DIALOG) {
    let updRes = await MPCH.updateOne({ id: mpch.id }, { partyInviter: null });
    if (updRes.nModified === 0) throw new Error('Dialog Invite NO: unable to update MPCH');
  }
  else if (btnid === BTNID_ACCEPT_PARTY_INVITE) {
    let updRes;
    updRes = await MPCH.updateOne({ id: mpch.id }, { partyInviter: null });
    if (updRes.nModified === 0) throw new Error('Dialog Invite OK-1: unable to update MPCH');

    let inviter = await MPCH.findOne({ displayName: ctx.request.body.dialogData.sender });
    inviter = inviter.displayName;
    if (inviter !== mpch.partyInviter) return ctx.throw(403, 'Bad inviter');

    updRes = await MPCH.updateOne({ id: mpch.id }, { partyLeader: inviter });
    if (updRes.nModified === 0) throw new Error('Dialog Invite OK: unable to update MPCH');
  }
});

module.exports = router;
