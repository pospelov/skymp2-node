const fs = require('fs');
const { expect } = require('chai');

const espm = require('./espm');

let skyrimEsm = espm.read(fs.readFileSync('./plugins/Skyrim.esm'));
let esp = espm.read(fs.readFileSync('./testfiles/Test1.esp'));

describe('ESM/ESP files reader', () => {
  it('reads REFR to TreeFloraThistle01 in Tamriel:Riverwood', () => {
    let refr = skyrimEsm.lookupRecord(0x2ae8e);
    expect(refr.NAME.id).to.equal(0xb91f0);
    expect(refr.NAME.espm).to.equal('Skyrim.esm');
    expect(refr.XSCL).to.equal(1.13);
    expect(refr.DATA).to.deep.equal({
      pos: [20148.1270, -45945.9727, -148.7056],
      rot: [-1.9313, -5.1825, 185.2435]
    });

    expect(refr.getParentExteriorWorldGroup().label == 0x3c);
    let riverwood = skyrimEsm.lookupRecord(refr.getParentCellGroup().label);
    expect(riverwood.id == 0x9732);
    expect(riverwood.getParentExteriorWorldGroup().label == 0x3c);
  });

  it('reads REFR to TreeFloraThistle01 in Tamriel:Riverwood (MODIFIED)', () => {
    let refr = esp.lookupRecord(0x2ae8e);
    expect(refr.NAME.id).to.equal(0xb91f0);
    expect(refr.NAME.espm).to.equal('Skyrim.esm');
    expect(refr.XSCL).to.equal(0.83);
    expect(refr.DATA).to.deep.equal({
      pos: [17950.7695, -44510.7734, -69.6368],
      rot: [8.6716, -9.7490, 19.6299]
    });

    expect(refr.getParentExteriorWorldGroup().label == 0x3c);
    let riverwood = skyrimEsm.lookupRecord(refr.getParentCellGroup().label);
    expect(riverwood.id == 0x9732);
    expect(riverwood.getParentExteriorWorldGroup().label == 0x3c);
  });
});
