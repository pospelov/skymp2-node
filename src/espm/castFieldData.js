const assert = require('assert');

function absCeil(x) {
  return Math.round(x * 10000) / 10000;
};

function parseFormId(fieldData, offset) {
  return {
    espm: 'Unknown', // will be calculated in _lazyFormatRecord(), index.js
    id: fieldData.readUInt32LE(offset ? offset : 0)
  };
}

module.exports = function(field, record) {
  if (field.type == 'EDID' || field.type == 'CNAM' || field.type == 'SNAM' || field.type == 'MAST') {
    field.data = field.data.toString('ascii', 0, field.data.length - 1);
  }
  else if (field.type == 'NAME') {
    if (record.type == 'REFR' || record.type == 'ACHR') { // base form
      field.data = parseFormId(field.data);
    }
  }
  else if (field.type == 'XSCL') { // scale
    field.data = Math.ceil(field.data.readFloatLE(0) * 100) / 100;
  }
  else if (field.type == 'DATA') {
    if (record.type == 'REFR' || record.type == 'ACHR') {
      field.data = {
        pos: [field.data.readFloatLE(0), field.data.readFloatLE(4), field.data.readFloatLE(8)],
        rot: [field.data.readFloatLE(12), field.data.readFloatLE(16), field.data.readFloatLE(20)],
      };
      const magicNumber = 572957.795131; // 180 * 10000 / Math.PI
      for (let i = 0; i < 3; ++i) {
        field.data.pos[i] = absCeil(field.data.pos[i]);
        field.data.rot[i] = Math.round(field.data.rot[i] * magicNumber) / 10000; // convert radians to degrees
      }
    }
    else if (record.type == 'GMST') {
      let edid = record.EDID;
      //if (edid == 'fTemperingSkillUseMult') console.log(edid, record.id.toString(16))

      if (edid[0] == 'b') field.data = !!field.data.readUInt32LE(0);
      else if (edid[0] == 'i') field.data = field.data.readUInt32LE(0);
      else if (edid[0] == 'f') field.data = field.data.readFloatLE(0);
      else if (edid[0] == 's') field.data = null; // not implemented
    }
  }
  else if (field.type == 'HEDR') { // TES4
    field.data = {
      version: Math.round(field.data.readFloatLE(0) * 100) / 100,
      numRecords: field.data.readInt32LE(4),
      nextObjectId: field.data.readUInt32LE(8)
    }
  }
  else if (field.type == 'PFIG') {
    if (record.type == 'TREE' || record.type == 'FLOR') {
      field.data = parseFormId(field.data);
    }
  }
  else if (field.type == 'XTEL') { // teleport
    field.data = {
      destination: parseFormId(field.data),
      pos: [field.data.readFloatLE(4), field.data.readFloatLE(8), field.data.readFloatLE(12)],
      rot: [field.data.readFloatLE(16), field.data.readFloatLE(20), field.data.readFloatLE(24)]
    };
  }
  else if (field.type == 'XCNT') { // count
    field.data = field.data.readUInt32LE(0);
  }
  else if (field.type == 'LVLD') { // Chance None (0 .. 100)
    field.data = field.data.readInt8(0);
  }
  else if (field.type == 'LVLF') { // Flags
    let flags = field.data.readInt8(0);
    field.data = {
      allLevels: !!(flags & 0x01), // (sets it to calculate for all entries < player level, choosing randomly from all the entries under)
      each: !!(flags & 0x02), // (sets it to repeat a check every time the list is called (if it's called multiple times), otherwise it will use the same result for all counts.)
      useAll: !!(flags & 0x04), // (use all entries when the list is called)
      specialLoot: !!(flags & 0x08)
    };
  }
  else if (field.type == 'LVLG') { // 	Points to a global record. If this is set, the value of the global is used instead of LVLD for chance none.
    field.data = parseFormId(field.data);
  }
  else if (field.type == 'LLCT') { // The number of entries in the list (the number of LVLO entries, essentially).
    field.data = field.data.readInt8(0);
  }
  else if (field.type == 'LVLO') { // leveled object
    field.data = {
      level: field.data.readUInt32LE(0),
      form: parseFormId(field.data, 4),
      count: field.data.readUInt32LE(8)
    };
  }
  else if (field.type == 'CNTO') {
    field.data = {
      form: parseFormId(field.data, 0),
      count: field.data.readUInt32LE(4)
    };
  }
}
