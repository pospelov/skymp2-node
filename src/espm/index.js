const assert = require('assert');

const castFieldData = require('./castFieldData');

const FLAG_COMPRESSED = 0x00040000;

class Record {
  constructor(parentRecord) {
    this.parentRecord = parentRecord;
  }
  getParentCellGroup() {
    let p = this.getPath();
    for (let i = 0; i != p.length; ++i) {
      let el = p[i];
      if (el.groupType == 'Cell Temporary Children' || el.groupType == 'Cell Children') return el;
    }
  }
  getParentExteriorWorldGroup() {
    let p = this.getPath();
    for (let i = 0; i != p.length; ++i) {
      let el = p[i];
      if (el.groupType == 'World Children') return el;
    }
  }
  getPath() {
    let path = [];
    let currentRecord = this;
    while (currentRecord.parentRecord) {
      path.push(currentRecord.parentRecord);
      currentRecord = currentRecord.parentRecord;
    }
    return path;
  }
}

// https://stackoverflow.com/questions/5432967/how-to-iterate-over-inner-objects-property-in-an-object
var forEachProperty = function (obj, callback) {
    for (var property in obj) {
        if (obj.hasOwnProperty(property) && obj[property] != null) {
            if (obj[property].constructor == Object) {
                forEachProperty(obj[property], callback);
            } else if (obj[property].constructor == Array) {
                for (var i = 0; i < obj[property].length; i++) {
                    forEachProperty(obj[property][i], callback);
                }
            } else {
                callback(obj, property, obj[property]);
            }
        }
    }
}

class MergedMaster {
  constructor(masters) {
    this.masters = masters;
  }
  lookupRecord(id, file) {
    if (typeof id !== 'number') throw new TypeError('number expected');
    if (typeof file !== 'string') throw new TypeError('string expected');

    id = id % 0x01000000;
    let resultRec = null;
    for (let i = 0; i < this.masters.length; ++i) {
      let master = this.masters[i];
      let rec = master.lookupRecord(id);
      this._lazyFormatRecord(rec);
      if (rec && rec.espm === file) resultRec = rec;
    }
    return resultRec;
  }
  _lazyFormatRecord(rec) {
    if (!rec) return;
    if (typeof rec.espm == 'number') {
      rec.espm = this.masters[rec.espm].name;
    }
    if (rec.cell && typeof rec.cell.espm == 'number') {
      rec.cell.espm = this.masters[rec.cell.espm].name;
    }
    if (rec.worldspace && typeof rec.worldspace.espm == 'number') {
      rec.worldspace.espm = this.masters[rec.worldspace.espm].name;
    }
    forEachProperty(rec, (obj, prop, val) => {
      if (prop === 'espm' && val === 'Unknown') {
        let index = Math.floor(obj.id / 0x01000000);
        obj.espm = this.masters[index].name;
        obj.id = obj.id % 0x01000000;
      }
    });
  }
}

class Master {
  constructor(topRecords, contextCache, name) {
    this.topRecords = topRecords;
    this.contextCache = contextCache;
    this.name = name;
  }
  lookupRecord(id) {
    return this.contextCache.recByShortId[id];
  }
  mergeWithPlugins(masters) {
    masters.unshift(this);
    return new MergedMaster(masters);
  }
}

class ReadContext {
  constructor(buf, pluginName) {
    this.buf = buf;
    this.pluginName = pluginName;
    this.pos = 0;
    this.cache = {
      recById: [],
      recByShortId: []
    };
  }
  readGroupHeader(rec) {
    const groupTypes = [
      ['Top (Type)',                      () => this.buf.toString('ascii', this.pos, this.pos + 4)],
      ['World Children',                  () => this.buf.readUInt32LE(this.pos)],
      ['Interior Cell Block',             () => this.buf.readInt32LE(this.pos)],
      ['Interior Cell Sub-Block',         () => this.buf.readInt32LE(this.pos)],
      ['Exterior Cell Block',             () => new Object({ y: this.buf.readInt16LE(this.pos), x: this.buf.readInt16LE(this.pos + 2)})],
      ['Exterior Cell Sub-Block',         () => new Object({ y: this.buf.readInt16LE(this.pos), x: this.buf.readInt16LE(this.pos + 2)})],
      ['Cell Children',                   () => this.buf.readUInt32LE(this.pos)],
      ['Topic Children',                  () => this.buf.readUInt32LE(this.pos)],
      ['Cell Persistent Childen',         () => this.buf.readUInt32LE(this.pos)],
      ['Cell Temporary Children',         () => this.buf.readUInt32LE(this.pos)],
      ['Cell Visible Distant Children',   () => this.buf.readUInt32LE(this.pos)]
    ];
    let groupTypeIndex = this.buf.readUInt32LE(this.pos + 4);
    rec.label = groupTypes[groupTypeIndex][1]();
    rec.groupType = groupTypes[groupTypeIndex][0];
    this.pos += 8;

    let day = this.buf.readUInt8(this.pos++);
    let months = this.buf.readUInt8(this.pos++);
    let date = new Date(2002 + months / 12, months % 12, day);
    rec.stamp = date.toDateString();

  }
  readRecordHeader(rec) {
    rec.flags = this.buf.readUInt32LE(this.pos); this.pos += 4;
    rec.id = this.buf.readUInt32LE(this.pos); this.pos += 4;
    rec.espm = null;
    if (rec.id === 0) return;

    rec.espm = Math.floor(rec.id / 0x01000000);

    let shortId = rec.id % 0x01000000;
    this.cache.recById[rec.id] = rec;
    this.cache.recByShortId[shortId] = rec;
    rec.id = shortId;
  }
  readField(dataSizeOverride) {
    let field = {};
    field.type = this.buf.toString('ascii', this.pos, this.pos += 4);
    let dataSize = this.buf.readUInt16LE(this.pos); this.pos += 2;
    if (dataSize === 0 && dataSizeOverride) {
      dataSize = dataSizeOverride;
    }
    let typed = new Uint8Array(dataSize);
    for (let i = 0; i < dataSize; ++i) {
      typed[i] = this.buf.readUInt8(this.pos++);
    }
    field.data = Buffer.from(typed);
    return field;
  }
  readAny(parentRecord = null) {
    let rec = new Record(parentRecord);
    rec.type = this.buf.toString('ascii', this.pos, this.pos += 4);
    let dataSize = this.buf.readUInt32LE(this.pos); this.pos += 4;
    if (rec.type == 'GRUP') {
      this.readGroupHeader(rec);
      this.pos += 6;
      rec.subrecs = [];

      let end = this.pos + dataSize - 24;
      while (this.pos < end) {
        let subrec = this.readAny(rec);
        rec.subrecs.push(subrec);
      }
    }
    else {
      this.readRecordHeader(rec);
      this.pos += 8;

      if (rec.flags & FLAG_COMPRESSED) { // not implemented
        this.pos += dataSize;
      }
      else {
        let end = this.pos + dataSize;
        while (this.pos < end) {
          let field = this.readField(this.dataSizeOverride);
          delete this.dataSizeOverride;

          castFieldData(field, rec);
          if (rec[field.type] === undefined) {
            rec[field.type] = field.data;
          }
          else if (Array.isArray(rec[field.type])) {
            rec[field.type].push(field.data);
          }
          else {
            let prevValue = rec[field.type];
            rec[field.type] = [prevValue, field.data];
          }

          if (field.type == 'XXXX') {
            this.dataSizeOverride = field.data.readUInt32LE(0);
          }
        }
      }
    }

    if (!this.TES4) this.TES4 = rec;

    const types = ['REFR', 'ACHR'];
    if (types.includes(rec.type)) {
      let worldGroup = rec.getParentExteriorWorldGroup();
      let cellGroup = rec.getParentCellGroup();
      if (worldGroup) {
        let worldRec = this.cache.recById[worldGroup.label];
        rec.worldspace = { espm: worldRec.espm, id: worldRec.id };
      }
      if (cellGroup) {
        let cellRec = this.cache.recById[cellGroup.label];
        rec.cell = { espm: cellRec.espm, id: cellRec.id };
      }
    }
    return rec;
  }
};

let espm = {};

espm.read = function (buf, pluginName) {
  let context = new ReadContext(buf, pluginName);
  let records = [];
  try {
    while (1) records.push(context.readAny());
  }
  catch(e) {
    if (e.toString().includes('RangeError')) return new Master(records, context.cache, pluginName);
    throw e;
  }
};

module.exports = espm;
