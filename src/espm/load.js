const path = require('path');
const fs = require('fs');

const espm = require('.');

function loadOne(dataFolder, pluginName) {
  let key = JSON.stringify([dataFolder, pluginName]);
  if (!this.contentCache) this.contentCache = {};
  if (!this.contentCache[key]) {
    let buf = fs.readFileSync(path.join(dataFolder, pluginName));
    this.contentCache[key] = espm.read(buf, pluginName);
  }
  return this.contentCache[key];
}

module.exports = function (dataFolder, espms) {
  if (!Array.isArray(espms)) throw new Error('Array of espm names expected');
  if (!espms) return;
  espms = JSON.parse(JSON.stringify(espms));
  let content = loadOne(dataFolder, espms[0]);
  espms.shift();
  espms = espms.map(pluginName => loadOne(dataFolder, pluginName));
  return content.mergeWithPlugins(espms);
};
