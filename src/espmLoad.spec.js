const fs = require('fs');
const { expect } = require('chai');

const load = require('./espm/load');

let skyrim = load('./plugins', ['Skyrim.esm']);
let dragonborn = load('./plugins', ['Skyrim.esm', 'Update.esm', 'Dragonborn.esm']);

describe('Merged ESP/ESM files reader', () => {
  it('reads ARMO SkinNakedFar flags changes', () => {
    let skinNakedFar = skyrim.lookupRecord(0x00040731, 'Skyrim.esm');
    let skinNakedFarDlc = dragonborn.lookupRecord(0x00040731, 'Skyrim.esm');
    expect(skinNakedFar.flags).not.to.equal(skinNakedFarDlc.flags);
  });

  it('reads ARMO SkinNakedFar as Skyrim.esm member with Dragonborn installed', () => {
    let skinNakedFarDlc = dragonborn.lookupRecord(0x00040731, 'Skyrim.esm');
    expect(skinNakedFarDlc.espm).to.equal('Skyrim.esm');
  });

  it('reads ALCH DLC2Sujamma as Dragonborn.esm member', () => {
    let dlc2Sujamma = dragonborn.lookupRecord(0x000207e6, 'Dragonborn.esm');
    expect(dlc2Sujamma.espm).to.equal('Dragonborn.esm');
  });

  it ('can not read ALCH DLC2Sujamma from Skyrim.esm', () => {
    expect(skyrim.lookupRecord(0x000207e6, 'Dragonborn.esm')).to.equal(null);
  });

  it('doesn\'t override records with same IDs from Skyrim.esm by Dragonborn.esm', () => {
    // Checks type because we have LAND record in Dragonborn.esm with same ID
    expect(dragonborn.lookupRecord(0x000136D4, 'Skyrim.esm').type).to.equal('ARMO');
  });

  it('reads ALCH reference and base from Dragonborn.esm', () => {
    let ref = dragonborn.lookupRecord(0x0002b154, 'Dragonborn.esm');
    expect(ref.type).to.equal('REFR');
    expect(ref.espm).to.equal('Dragonborn.esm');
    expect(ref.NAME.espm).to.equal('Dragonborn.esm');
    let base = dragonborn.lookupRecord(ref.NAME.id, ref.NAME.espm);
    expect(base.type).to.equal('ALCH');
    expect(base.espm).to.equal('Dragonborn.esm')
  });

  it('reads DOOR reference from Dragonborn.esm with correct XTEL', () => {
    let ref = dragonborn.lookupRecord(0x00018684, 'Dragonborn.esm');
    let teleportTargetRef = dragonborn.lookupRecord(ref.XTEL.destination.id, ref.XTEL.destination.espm);
    expect(teleportTargetRef.cell.espm).to.equal('Dragonborn.esm');
  });
});
