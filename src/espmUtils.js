const utils = require('./utils');

// TODO: better support of flags, support character level
function evalLeveledItem(lvli, mergedMaster) {
  let entries = [];
  if (lvli.LVLF.useAll) {
    console.log('useall');
    for (let i = 0; i < lvli.LVLO.length; ++i) {
      entries.push({ form: lvli.LVLO[i].form, count: lvli.LVLO[i].count });
    }
  }
  else {
    if (Array.isArray(lvli.LVLO)) {
      let i = utils.randomInteger(0, lvli.LVLO.length - 1);
      entries.push({ form: lvli.LVLO[i].form, count: lvli.LVLO[i].count });
    }
    else entries.push({ form: lvli.LVLO.form, count: lvli.LVLO.count });
  }
  let subLvli = [];
  entries = entries.filter(entry => {
    let rec = mergedMaster.lookupRecord(entry.form.id, entry.form.espm);
    if (rec.type === 'LVLI') {
      subLvli.push(rec);
      return false;
    }
    return true;
  });
  subLvli.map(x => evalLeveledItem(x, mergedMaster)).forEach(entryArray => {
    entryArray.forEach(entry => entries.push(entry));
  });
  return entries;
};

module.exports.evalLeveledItem = evalLeveledItem;
