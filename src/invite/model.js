const mongoose = require('mongoose');

// https://stackoverflow.com/questions/1349404/generate-random-string-characters-in-javascript
function makeid() {
  var text = "";
  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

  for (var i = 0; i < 8; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));

  return text;
}

let schema = new mongoose.Schema({
  text: { type: String, required: true }
});

schema.statics.reserve = async function(n) {
  let docs = await this.find({});
  if (docs.length < n) {
    let array = [];
    for (let i = 0; i < n - docs.length; ++i) {
      array.push({ text: makeid() });
    }
    return await this.insertMany(array);
  }
  return [];
}

schema.statics.use = async function(text) {
  return await this.findOneAndDelete({ text });
}

module.exports =  mongoose.model('Invite', schema);
