const Router = require('koa-router');
const mongoose = require('mongoose');
const { MPCH } = require('../records');

let router = new Router;

router.get('/:token', async (ctx) => {
  let mpch = await MPCH.findOne({ token: ctx.params.token });
  if (!mpch) return ctx.throw(404, 'MPCH not found');

  if (!mpch.partyLeader) return ctx.throw(404, 'Party not found');

  let party = await MPCH.find({ partyLeader: mpch.partyLeader });

  let res = { members: [], _names: {} };
  for (let i = 0; i < party.length; ++i) {
    let member = party[i];
    res.members.push(0xff000000 + member.id);
    if (member.displayName == mpch.partyLeader) res.leader = 0xff000000 + member.id;
    res._names[i.toString()] = member.displayName;
  }

  ctx.body = res;
});

module.exports = router;
