const crypto = require('crypto');
const mongoose = require('mongoose');

const utils = require('../../utils');

let schema = require('../ACHR/model').schema.clone();

schema.add({
  displayName: { type: String, index: true, unique: true },
  passwordHash: String,
  salt: String,
  token: { type: String, index: true, unique: true },
  markers: { type: [{ id: Number, espm: String }], required: true, default: [] },
  look: {
    isFemale: Boolean,
    raceID: Number,
    weight: Number,
    skinRGB: [Number],
    hairRGB: [Number],
    headparts: [Number],
    options: [Number],
    presets: [Number],
    tints: [[Number]],
    headTextureSet: Number
  },
  eq: {
    rightHand: Number,
    leftHand: Number,
    armor: [Number]
  },
  partyLeader: { type: String, index: true },
  partyInviter: String,
  confirmCode: String,
  emailConfirmed: Boolean,
  resetPassCode: String,
  email: { type: String, required: true, unique: true, index: true },
  lastLogin: Date,
  lastUpdate: { type: Date, index: true },
  secondsPlayed: { type: Number, required: true, default: 0 },
  crashes: [{
    lastLogin: Date,
    lastUpdate: Date,
    version: String,
    isNormalExit: Boolean
  }],
  exchangeEvent: mongoose.Schema.Types.Mixed,
  scriptVars: mongoose.Schema.Types.Mixed,
  scriptMessages: [{
    from: String,
    header: String,
    content: String
  }],
  finalClosedBetaActivated: Boolean,
});

schema.methods.setPassword = function (password) {
  this.salt = crypto.randomBytes(128).toString('base64');
  this.passwordHash = crypto.pbkdf2Sync(password, this.salt, 1, 128, 'sha1')
}

schema.methods.checkPassword = function (password) {
  return crypto.pbkdf2Sync(password, this.salt, 1, 128, 'sha1') == this.passwordHash;
};

schema.methods.newToken = function() {
  this.token = 'skymp' + Math.random().toString(36).substr(2);
};

schema.methods.toPublic = function() {
  let res = this.toObject();
  delete res.passwordHash;
  delete res.salt;
  res.inv = this.formatInv();
  return res;
};

schema.statics.findOnline = async function () {
  return await this.find({ lastUpdate: { $gte: Date.now() - this.getTimeoutMs() } });
};

schema.statics.getTimeoutMs = function() {
  return 10 * 1000;
}

schema.statics.getSpawnPoint = function () {
  let solstheim = { espm: 'Dragonborn.esm', id: 0x800 };
  let points = [
    [28717.83984375, 33150.37109375, 338.5165710449219] // raven rock
    //[17224.7734, -47204.4531, -51.8551], // riverwood
    //[-66330.3281, 94437.9531, -13655.2490], //solitude
    //[166912.0000, -88000.0000, 11076.0000], // riften
    //[-163653.8281, 5266.4155, -4520.0000], // markarth
    //[135027.2188, 26062.9199, -12564.8174], // windhelm
    //[110381.2891, 100399.5391, -9032.0000], // winterhold
    //[-37371.2227, 75986.2500, -13505.7227], // morthal
    //[-92711.3281, 94884.0469, -10511.4785] // dragon bridge
  ];
  let index = utils.randomInteger(0, points.length - 1);
  return { pos: points[index], worldspace: solstheim };
}

module.exports = mongoose.model('MPCH', schema);
