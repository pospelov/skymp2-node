const mongoose = require('mongoose');
const utils = require('../../utils');
const espmUtils = require('../../espmUtils');

let schema = require('../record').schema.clone();

schema.add({
  EDID: String,
  NAME: { espm: String, id: Number },
  DATA: {
    pos: [Number],
    rot: [Number]
  },
  XSCL: Number,
  XTEL: {
    destination: { espm: String, id: Number },
    pos: [Number],
    rot: [Number]
  },
  XCNT: { type: Number, required: true, default: 1 },
  cell: { espm: String, id: Number },
  worldspace: { espm: String, id: Number },
  isOpen: { type: Boolean, default: false, required: true },
  inv: {
    simple: mongoose.Schema.Types.Mixed
  },

  // Store duplicate of base.type to speed up db operations
  baseType: String,

  // For containers
  invCalculated: { type: Boolean, default: false, required: true },

  despawnTimeMs: { type: Number, default: 5 * 60 * 1000, required: true }
});

function toEntry(src) {
  return { count: src.count, form: { espm: src.form.espm, id: src.form.id } };
}

function formToKey(form) {
  return (form.espm + ':' + form.id.toString(16)).replace('.', '-');
}

function keyToForm(key) {
  return {
    espm: key.split(':')[0].replace('-', '.'),
    id: parseInt(key.split(':')[1], 16)
  };
}

schema.statics.minifyEntry = function (entry) {
  return {
    form: { id: entry.form.id, espm: entry.form.espm },
    count: entry.count
  };
};

schema.statics.addItem = async function (filter, entries) {
  filter = JSON.parse(JSON.stringify(filter));
  if (!Array.isArray(entries)) entries = [entries];
  if (!entries.length) return;
  let op = { $inc: {}, invCalculated: true };
  entries.forEach(entry => {
    entry = this.minifyEntry(entry);
    let key = 'inv.simple.' + formToKey(entry.form);
    if (!op.$inc[key]) op.$inc[key] = 0;
    op.$inc[key] += +entry.count;
  });
  if ((await this.updateOne(filter, op)).nModified !== 1) throw new Error('addItem failed');
};

schema.statics.removeItem = async function (filter, entries) {
  filter = JSON.parse(JSON.stringify(filter));
  if (!Array.isArray(entries)) entries = [entries];
  if (!entries.length) return;
  let op = { $inc: {} };
  entries.forEach(entry => {
    let key = 'inv.simple.' + formToKey(entry.form);
    if (!op.$inc[key]) op.$inc[key] = 0;
    op.$inc[key] += -(+entry.count);
    filter[key] = { $gte: (+entry.count) };
  });
  let updRes = await this.updateOne(filter, op);
  if (updRes.nModified !== 1) {
    throw new Error('remove item ' + JSON.stringify(entries) + ' (' + entry.count + ')  from ' + JSON.stringify(filter) + ' failed');
  }
}

schema.methods.formatInv = function() {
  let entries = [];
  for (let key in this.inv.simple) {
    entries.push({ form: keyToForm(key), count: this.inv.simple[key] });
  }
  return { entries };
};

schema.statics.getProduce = function (mergedMaster, base, count) {
  let isItem = utils.getItemTypes().includes(base.type);
  console.log('isItem:', isItem);
  console.log('base.type:', base.type);
  let entries = [{ form: isItem ? base : base.PFIG, count }];
  let item = mergedMaster.lookupRecord(entries[0].form.id, entries[0].form.espm);
  if (item.type === 'LVLI') {
    entries = espmUtils.evalLeveledItem(item, mergedMaster);
  }
  return entries;
};

schema.statics.pickUp = async function (targetFilter, token, destroy, mergedMaster, base, count) {
  const MPCH = mongoose.model('MPCH');
  const REFR = this;

  targetFilter.isOpen = false;
  targetFilter.$or = utils.getItemTypes().concat(['TREE', 'FLOR']).map(x => new Object({ baseType: x }));

  let entries = REFR.getProduce(mergedMaster, base, count || 1).map(x => REFR.minifyEntry(x));

  if (!destroy) {
    let updRes = await REFR.updateOne(targetFilter, { isOpen: true });
    if (updRes.nModified !== 1) return false;

    MPCH.addItem({ token }, entries);
    //entries = entries.map(x => REFR.minifyEntry(x));
    return entries
  }
  else {
    let rmRes = await REFR.findOneAndRemove(targetFilter);
    if (rmRes) {
      MPCH.addItem({ token }, entries);
      return entries;
    }
    else console.log('SUKA NE NASHEL BLYAT', targetFilter);
  }
  return null;
};

schema.statics.despawnIfNeed = async function (refr) {
  let age = Date.now() - refr._id.getTimestamp();
  if (age < refr.despawnTimeMs) return;
  if (!utils.getItemTypes().includes(refr.baseType)) return;

  if (refr.espm === '') {
    let { id, espm } = refr;
    await this.findOneAndRemove({ id, espm });
  }
};

module.exports = mongoose.model('REFR', schema);
