module.exports = {
  REFR: require('./REFR/model'),
  ACHR: require('./ACHR/model'),
  MPCH: require('./MPCH/model'),

  lookupById: async function(model, id, espm, mergedMaster) {
    if (typeof id !== 'number') throw new TypeError('number expected');
    if (typeof espm !== 'string') throw new TypeError('string expected');

    if (typeof model === 'string') model = this[model];
    if (!model) throw new Error('invalid model');

    console.log('trying to find', {id,espm})

    let result = await model.findOne({ espm: espm, id: id });
    if (result) return result;

    let rec = mergedMaster.lookupRecord(id, espm);
    if (!rec) throw new Error(`no sush record (${id.toString(16)}) in ${espm}`)
    if (model.modelName !== rec.type) throw new Error(`wrong record type ${rec.type}`);

    if (rec.type === 'REFR') rec.baseType = mergedMaster.lookupRecord(rec.NAME.id, rec.NAME.espm).type;

    return await model.create(rec);
  },

  lookupByIdMany: async function(model, formsArray, mergedMaster) {
    if (typeof model === 'string') model = this[model];
    if (!model) throw new Error('invalid model');

    if (formsArray.length === 0) return [];

    let filter = { $or: [] };
    formsArray.forEach(form => {
      filter.$or.push({ id: form.id, espm: form.espm });
    });
    let documents = await model.find(filter);
    let promises = [];
    if (documents.length < formsArray.length) {
      console.log('fuck up', documents.length);
      let dbFormKeys = new Set;
      let localFormKeys = new Set;
      documents.forEach(x => dbFormKeys.add(x.espm + ' ' + x.id));
      formsArray.forEach(x => localFormKeys.add(x.espm + ' ' + x.id));
      for (let k of localFormKeys) {
        if (!dbFormKeys.has(k)) {
          let f = async () => {
            try {
              return await require('.').lookupById('REFR', parseInt(k.split(' ')[1]), k.split(' ')[0], mergedMaster);
            }
            catch (e) {
              return null;
            }
          };
          promises.push(f());
        }
      }
    }
    if (promises.length) {
      documents = documents.concat(await Promise.all(promises));
    }
    return documents.filter(x => x !== null);
  }
};
