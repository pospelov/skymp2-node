const mongoose = require('mongoose');

let utils = require('../utils');

let recordSchema = new mongoose.Schema({
  espm: { type: String, index: true }, // !espm means 0xFF000000+
  id: { type: Number, required: true, index: true },
  flags: Number,
  type: { type: String, required: true, uppercase: true }
});

// TODO: rewrite it!
recordSchema.statics.createWithFormId = async function(fields, beginFrom) {
  beginFrom = beginFrom || 0;
  if (beginFrom < 0 || beginFrom > 0xD || Math.round(beginFrom) != beginFrom) {
    throw new Error(`bad beginFrom value ${beginFrom}`);
  }

  const STEP = 0x00100000;
  let id = beginFrom * STEP + utils.randomInteger(0, STEP - 1);

  while (1) {
    fields.id = id;
    try { return await this.create(fields); }
    catch (err) {
      console.log(err.toString());
      ++id;
    }
  }
};

recordSchema.methods.as = function(type) {
  return type === this.type ? this : null;
}

module.exports = { schema: recordSchema };
