const Router = require('koa-router');
const mongoose = require('mongoose');
const axios = require('axios');

const records = require('.');
const { MPCH, REFR } = records;
const utils = require('../utils');
const espmUtils = require('../espmUtils');
const config = require('../../config');

const MIN_PASSWORD_LENGTH = 6;

function sendConfirmCode(email, confirmCode) {
  let server = require("emailjs").server.connect({
    user: "skymp.official@gmail.com",
    password: "GoogleSuper",
    host: "smtp.gmail.com",
    ssl: true,
    port: 465// ?
  }); // send the message and get a callback with an error or details of the message that was sent

  let content = "Код подтверждения: " + confirmCode;
  let dialogue = `<html>Тебе просили кое-что передать. Лично в руки. Так, посмотрим...<br><b>${content}</b><br>Ну всё, мне пора.</html>`;
  server.send({
    text: dialogue,
    from: "Гонец от SkyMP <username@your-email.com>",
    to: `user <${email}>`,
    subject: 'А, вот ты где, а я тебя повсюду ищу',
    attachment: [{ data: dialogue, alternative: true }]
  }, function(err, message) { console.log('EMAILJS', err || message); });
}

class FormCache {
  constructor() {
    this.storage = {};
  }
  at(id, espm) {
    return this.storage[espm + id];
  }
  add(rec) {
    this.storage[rec.espm + rec.id] = rec;
  }
}

let router = new Router;
let gFormCache = new FormCache;

router.post('/MPCH', async (ctx) => {
  let { name, password, email, invite } = ctx.request.body;
  if (name.length < 2 || password.length < MIN_PASSWORD_LENGTH) return ctx.throw(411);

  let result = await MPCH.findOne({ displayName: name });
  if (result) return ctx.throw(409);
  result = await MPCH.findOne({ email });
  if (result) return ctx.throw(409);

  const Invite = require('../invite/model');
  let inviteObj = await Invite.use(invite);
  if (!inviteObj) return ctx.throw(402);

  let confirmCode = '' + utils.randomInteger(100000, 999999);

  let sp = MPCH.getSpawnPoint();
  result = await MPCH.createWithFormId({
    // Record:
    espm: '',
    type: 'MPCH',
    // REFR:
    DATA: { pos: sp.pos, rot: [0, 0, 0] },
    worldspace: sp.worldspace,
    cell: { espm: 'Skyrim.esm', id: 3444 },
    inv: {
      simple: { 'Skyrim-esm:5b6a1': 1, 'Skyrim-esm:c36e8': 1 } // Default clothes
    },
    // MPCH:
    displayName: name,
    confirmCode: confirmCode,
    emailConfirmed: false,
    email: email
  });
  result.setPassword(password);
  result.newToken();
  await result.save();
  ctx.body = result.toPublic();
  ctx.status = 201;

  sendConfirmCode(email, confirmCode);
});

router.post('/MPCH/confirm/retry', async (ctx) => {
  let { name } = ctx.request.body;
  let mpch = await MPCH.findOne({ displayName: name });
  if (!mpch) return ctx.throw(404, 'MPCH not found');
  if (mpch.emailConfirmed) return ctx.throw(403);
  sendConfirmCode(mpch.email, mpch.confirmCode);
  ctx.status = 204;
});

router.post('/MPCH/resetPass', async (ctx) => {
  let { email } = ctx.request.body;
  let mpch = await MPCH.findOne({ email });
  if (!mpch) return ctx.throw(404, 'MPCH not found');
  mpch.resetPassCode = '' + utils.randomInteger(1000000, 9999999); // TODO: add alphabet chars
  await mpch.save(); // TODO: update instead
  sendConfirmCode(mpch.email, mpch.resetPassCode);
  ctx.status = 204;
});

router.post('/MPCH/resetPass/finish', async (ctx) => {
  let { code, password } = ctx.request.body;
  let mpch = await MPCH.findOne({ resetPassCode: code });
  if (!mpch) return ctx.throw(404, 'MPCH not found');
  if (!mpch.resetPassCode || mpch.resetPassCode !== code) return ctx.throw(403);
  if (password.length < MIN_PASSWORD_LENGTH) return ctx.throw(411);
  mpch.resetPassCode = null; // TODO: use delete operator instead?
  mpch.setPassword(password);
  await mpch.save(); // TODO: update instead
  ctx.status = 204;
});

router.post('/MPCH/confirm', async (ctx) => {
  let { confirmCode, name } = ctx.request.body;
  let mpch = await MPCH.findOne({ displayName: name });
  let updRes = await MPCH.updateOne({ displayName: name, confirmCode: confirmCode }, { emailConfirmed: true });
  if (!updRes.nModified) return ctx.throw(403);
  ctx.status = 204;
});

router.get('/MPCH/:token', async (ctx) => {
  let result = await MPCH.findOne({ token: ctx.params.token });
  if (!result) result = await MPCH.findOne({ id: parseInt(ctx.params.token, 16) });
  if (!result) return ctx.throw(404, 'MPCH not found');

  ctx.body = result.toPublic();
});

router.put('/MPCH/:token', async (ctx) => {
  let fields = ctx.request.body.fields;
  for (let property in fields) {
    if (!fields.hasOwnProperty(property)) continue;
    let allowed = ['look', 'markers'];
    if (!allowed.includes(property)) return ctx.throw(403);
  }
  let updateRes = await MPCH.updateOne({ token: ctx.params.token }, fields);
  if (updateRes.n !== 1) return ctx.throw(404, 'MPCH not found');
  if (updateRes.nModified !== 1) return ctx.throw(400);

  ctx.status = 204;
});

const gitlabUrl = 'http://gitlab.com/pospelov/skymp2-binaries/raw/master';//'http://185.211.246.231:10080/root/skymp2-binaries/raw/master';
let binariesRepo = axios.create({ baseURL: gitlabUrl, timeout: 3000 });
let gVersionFull;

async function getClientVersionLoop() {
  while (1) {
    try {
      let txts = (await Promise.all([
        binariesRepo.get('/SKYMP2/version.txt'),
        binariesRepo.get('/SKYMP2/build-id.txt')
      ])).map(x => x.data);

      gVersionFull = txts[0] + '-' + txts[1];
    }
    catch (err) {
      console.error('getClientVersionLoop =>', err.toString());
    }
  }
};
getClientVersionLoop();

router.post('/MPCH/login', async (ctx) => {
  let result = await MPCH.findOne({ $or: [{ displayName: ctx.request.body.name }/*, { email: ctx.request.body.name }*/] });
  if (!result) return ctx.throw(404);
  if (!result.checkPassword(ctx.request.body.password)) return ctx.throw(403);
  if (!result.emailConfirmed) return ctx.throw(428);
  if (ctx.request.body.version !== gVersionFull) return ctx.throw(426);

  result.newToken();
  if (result.lastLogin && result.lastUpdate) {
    result.secondsPlayed += Math.ceil((result.lastUpdate - result.lastLogin) / 1000);
  }

  result.crashes.push({
    lastLogin: result.lastLogin,
    lastUpdate: result.lastUpdate,
    version: ctx.request.body.version,
    isNormalExit: !ctx.request.body.crashed
  });
  result.lastLogin = Date.now();
  result.lastUpdate = Date.now();

  let acti = result.finalClosedBetaActivated;
  if (!acti) {
    sp = MPCH.getSpawnPoint();
    result.look = null;
    result.DATA.pos = sp.pos;
    result.cell = null;
    result.worldspace = sp.worldspace;
  }
  result.finalClosedBetaActivated = true;
  await result.save();
  ctx.body = result.toPublic();

  if (!acti) {
    let count = 1;
    let items = [0x00013ED9, 0x00013EDA, 0x00013ED7, 0x00013EDB, 0x000135B8, 0x0001359D, 0x00013987, 0x000A6D7B, 0x000A6D7D, 0x000A6D7F, 0x0006F39E, 0x0009F25C];
    items.forEach(x => {
      MPCH.addItem({ displayName: result.displayName }, { form: { espm: 'Skyrim.esm', id: x }, count });
    });
    //items = [0x01CD93, 0x037564, 0x037563, 0x01CD92, 0x01CD94, 0x01CD95, 0x026234, 0x000A6D7B, 0x000A6D7D, 0x000A6D7F, 0x0006F39E, 0x0009F25C,
    //  0x039112, 0x03910e, 0x039110, 0x039114, 0x01CDAD, 0x01CDAF, 0x01CDB1];
    items.forEach(x => {
      //MPCH.addItem({ displayName: result.displayName }, { form: { espm: 'Dragonborn.esm', id: x }, count });
    });
  }
});

class MPCHUpdater {
  constructor() {
    this.data = [];
  }
  update(query, update) {
    this.data.push({ query, update });
  }
  tick() {
    return new Promise((resolve, reject) => {
      if (!this.data.length) return setImmediate(resolve);
      let bulk = MPCH.collection.initializeUnorderedBulkOp();
      this.data.forEach(data => { bulk.find(data.query).update(data.update); });
      this.data = [];
      bulk.execute(err => { err ? reject(err) : resolve(); });
    });
  }
  async loop() {
    while (1) {
      try {
        // Save players every 5000ms
        // Without sleep event loop performance will be decreased
        await utils.sleep(5000);
        await this.tick();
      }
      catch (err) {
        console.error('bulk.execute error:', err);
      }
    }
  }
}

let mpchUpdater = new MPCHUpdater;
mpchUpdater.loop();

router.post('/MPCH/update/:token', async (ctx) => { // TODO: anti-flood
  let { mov, eq } = ctx.request.body;
  let radius = 3000;
  mpchUpdater.update({
    token: ctx.params.token,
    'DATA.pos.0': { $gt: mov.pos[0] - radius, $lt: mov.pos[0] + radius },
    'DATA.pos.1': { $gt: mov.pos[1] - radius, $lt: mov.pos[1] + radius },
    'DATA.pos.2': { $gt: mov.pos[2] - radius, $lt: mov.pos[2] + radius }
   }, { '$set': { lastUpdate: Date.now(), 'DATA.pos': mov.pos, 'DATA.rot': mov.rot, eq }});
  ctx.status = 204;
});

router.post('/MPCH/cellhost/:token', async (ctx) => {
  ctx.body = [config.cellhostAddress];
});

router.get('/:espm/:model/:formkey', async (ctx) => {
  try {
    let formid = parseInt(ctx.params.formkey, 16);
    if (Number.isNaN(formid)) return ctx.throw(400, 'Bad formid');
    ctx.body = await records.lookupById(ctx.params.model, formid, ctx.params.espm, ctx.mergedMaster);
  }
  catch (err) {
    ctx.throw(404, err.toString());
  }
});

router.post('/deprecated/activate/:token', async (ctx) => {
  let { id, espm, typeId, setOpen } = ctx.request.body;

  const TYPE_CONT = 28;
  const TYPE_DOOR = 29;
  const TYPE_TREE = 38;
  const TYPE_FLORA = 39;

  console.log('acti', id, espm);

  let ref = gFormCache.at(id, espm) || await records.lookupById('REFR', id, espm, ctx.mergedMaster);
  console.log('ref.NAME:',ref.NAME);
  let base = ctx.mergedMaster.lookupRecord(ref.NAME.id, ref.NAME.espm);
  console.log('base', base.espm, base.id, base.type);

  if (typeId === TYPE_CONT) {
    let updRes = await REFR.updateOne({ id, espm, invCalculated: false }, { invCalculated: true });
    if (updRes.nModified > 0) {
      // Calculate inventory
      let calculatedEntries = [];
      if (!Array.isArray(base.CNTO)) base.CNTO = [base.CNTO];
      for (let i = 0; i < base.CNTO.length; ++i) {
        let entry = base.CNTO[i];
        let item = ctx.mergedMaster.lookupRecord(entry.form.id, entry.form.espm);
        if (item.type === 'LVLI') {
          let evalres = espmUtils.evalLeveledItem(item, ctx.mergedMaster);
          calculatedEntries = calculatedEntries.concat(evalres);
        }
        else calculatedEntries.push(entry);
      }
      await REFR.addItem({ id, espm }, calculatedEntries);
      ctx.body = { activate: true, targetInv: { entries: calculatedEntries } };
    }
    else {
      console.log(ref.inv);
      ctx.body = { activate: true, targetInv: ref.formatInv() };
    }
    return;
  }

  if (typeId === TYPE_DOOR) {
    if (ref.XTEL && ref.XTEL.pos.length > 0) {
      console.log('Teleprot Door');
      let dest = ref.XTEL.destination;
      dest = gFormCache.at(dest.id, dest.espm) || await records.lookupById('REFR', dest.id, dest.espm, ctx.mergedMaster);
      await MPCH.updateOne({ token: ctx.params.token }, { cell: dest.cell, worldspace: dest.worldspace, 'DATA.pos': ref.XTEL.pos, 'DATA.rot': ref.XTEL.rot });
      ctx.body = { activate: true };
    }
    else {
      let updRes = await REFR.updateOne({ id: id, espm: espm, baseType: 'DOOR' }, { isOpen: setOpen });
      ctx.body = { isOpen: setOpen };
    }
    return;
  }

  // TREE || FLOR || any item
  let destroy = espm === '' && typeId !== TYPE_TREE && typeId !== TYPE_FLORA;
  let entries = await REFR.pickUp({ id, espm }, ctx.params.token, destroy, ctx.mergedMaster, base, ref.XCNT);
  console.log('entries:', entries);
  if (entries) {
    ctx.body = { entries, activate: base.type !== 'BOOK' };
    return;
  }

  ctx.throw(400, 'Unable to activate ' + typeId);
});

class FFFinder {
  constructor() {
    this.results = [];
    this.loop();
  }
  async loop() {
    while (1) {
      await utils.sleep(100);
      this.promise = REFR.find({ espm: '' });
      this.results = await this.promise;
    }
  }
  async find(pos, cellOrWorld, radius) {
    await this.promise;
    this.results.forEach(refr => REFR.despawnIfNeed(refr));
    return this.results.filter(x => {
      for (let i = 0; i < 3; ++i) {
        if (x.DATA.pos[i] < pos[i] - radius || x.DATA.pos[i] > pos[i] + radius) return false;
      }
      let cellMatch = x.cell.id === cellOrWorld.id && x.cell.espm === cellOrWorld.espm;
      let worldMatch = x.worldspace.id == cellOrWorld.id && x.worldspace.espm === cellOrWorld.espm;
      return cellMatch || worldMatch;
    });
  }
}
let gFinderFF = new FFFinder;

let gRequestDataCache = {};

router.post('/deprecated/requestdata', async (ctx) => {
  let requestedRefs = ctx.request.body;

  console.log('nonFF Size:', requestedRefs.length);

  let pos = requestedRefs[requestedRefs.length - 1].pos;
  requestedRefs.pop();

  let cellOrWorld = requestedRefs[requestedRefs.length - 1];
  requestedRefs.pop();

  let cacheKey = pos[0] + ' '+  pos[1] + ' ' + pos[2] + ' ' + cellOrWorld;

  let results = gRequestDataCache[cacheKey];
  if (!results) {
    results = await records.lookupByIdMany('REFR', requestedRefs, ctx.mergedMaster);
    results.forEach(ref => gFormCache.add(ref));

    gRequestDataCache[cacheKey] = results;
    setTimeout(() => {
      gRequestDataCache[cacheKey] = null;
    }, 5000);
  }

  // Find 0xFF refrs
  let resultsFF = await gFinderFF.find(pos, cellOrWorld, 7000);

  console.log('results', results.length);

  ctx.body = resultsFF.concat(results);
});

router.post('/deprecated/invevent/:token', async (ctx) => {
  let ie = ctx.request.body;

  if (ie.type === 'drop') delete ie.ref;
  else ie.ref = gFormCache.at(ie.ref.id, ie.ref.espm) || await records.lookupById('REFR', ie.ref.id, ie.ref.espm, ctx.mergedMaster);

  switch (ie.type) {
    case 'drop': {
      let mpch = await MPCH.findOne({ token: ctx.params.token });
      await MPCH.removeItem({ token: ctx.params.token }, ie.entry);
      let angle = ie.movement.rot[2] + utils.randomInteger(0, 40) - 20;
      ie.movement.pos[0] += Math.sin(angle / 180 * Math.PI) * 64;
      ie.movement.pos[1] += Math.cos(angle / 180 * Math.PI) * 64;
      ie.movement.pos[2] += 8;
      let newRefr = await REFR.createWithFormId({
        // Record:
        espm: '',
        type: 'REFR',
        // REFR:
        DATA: { pos: ie.movement.pos, rot: [0,0,0] },
        NAME: ie.entry.form,
        XCNT: ie.entry.count,
        worldspace: mpch.worldspace,
        cell: mpch.cell,
        inv: { simple: {} },
        baseType: ctx.mergedMaster.lookupRecord(ie.entry.form.id, ie.entry.form.espm).type,
        isOpen: false
      }, 0x1);
      ctx.status = 204;
      console.log('drop:', newRefr.id)
      break;
    }
    case 'take': {
      await REFR.removeItem({ id: ie.ref.id, espm: ie.ref.espm }, ie.entry);
      MPCH.addItem({ token: ctx.params.token }, ie.entry);
      ie.ref = await REFR.findById(ie.ref._id);
      ctx.body = { refInv: ie.ref.formatInv() };
      break;
    }
    case 'put': {
      await MPCH.removeItem({ token: ctx.params.token }, ie.entry);
      REFR.addItem({ id: ie.ref.id, espm: ie.ref.espm }, ie.entry);
      ie.ref = await REFR.findById(ie.ref._id);
      ctx.body = { refInv: ie.ref.formatInv() };
      break;
    }
  }
});

router.post('/deprecated/sendinteractionclick/:token', async (ctx) => {
  let mpch = await MPCH.findOne({ token: ctx.params.token });
  if (!mpch) return ctx.throw(404, 'MPCH not found');

  const INDEX_PARTY_MAKELEADER = 1001;
  const INDEX_PARTY_KICK_OR_LEAVE = 1000;
  const INDEX_WHISPER = 2;
  const INDEX_EXCHANGE = 1;
  const INDEX_INVITE = 0;

  let { targetid, index } = ctx.request.body;

  let target = await MPCH.findOne({ id: targetid - 0xFF000000 });
  if (!target) return ctx.throw(404, 'target MPCH not found');

  if (index === INDEX_PARTY_MAKELEADER) {
    await MPCH.updateMany({ partyLeader: mpch.displayName }, { partyLeader: target.displayName })
    await MPCH.updateMany({ partyInviter: mpch.displayName }, { partyInviter: target.displayName })
  }
  else if (index === INDEX_PARTY_KICK_OR_LEAVE) {
    let isLeader = mpch.partyLeader == mpch.displayName;
    await MPCH.updateOne({ id: isLeader ? target.id : mpch.id}, { partyLeader: null });
  }
  else if (index === INDEX_WHISPER) {

  }
  else if (index === INDEX_EXCHANGE) {

  }
  else if (index === INDEX_INVITE) {
    if (target.partyLeader && (await MPCH.find({ partyLeader: target.partyLeader })).length > 1) return ctx.throw(403, 'Already in party');
    //if (target.partyInviter) return ctx.throw(403, 'Already invited');

    await MPCH.updateOne({ id: mpch.id }, { partyLeader: mpch.displayName });
    await MPCH.updateOne({ id: target.id }, { partyInviter: mpch.displayName });

    ctx.body = { action: 'broadcastPartyInvite' };
  }
});

router.get('/:espm/:formid', async (ctx) => {
  let names = mongoose.modelNames();
  for (let i = 0; i < names.length; ++i) {
    let success;
    try {
      let formid = parseInt(ctx.params.formid, 16);
      ctx.body = await records.lookupById(names[i], formid, ctx.params.espm, ctx.mergedMaster);
      success = true;
    }
    catch (err) {
      success = false;
    }
    if (success) {
      if (ctx.body.inv && ctx.body.formatInv) {
        let inv = ctx.body.formatInv();
        ctx.body = ctx.body.toObject();
        ctx.body.inv = inv;
      }
      return;
    }
  }
  ctx.throw(404);
});

module.exports = router;
