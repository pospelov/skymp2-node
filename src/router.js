const moment = require('moment');
const Router = require('koa-router');
let router = new Router;

let route;

route = require('./records/router');
router.use('/records', route.routes(), route.allowedMethods());

route = require('./party/router');
router.use('/party', route.routes(), route.allowedMethods());

route = require('./dialog/router');
router.use('/dialog', route.routes(), route.allowedMethods());

route = require('./api/router');
router.use('/api', route.routes(), route.allowedMethods());

router.get('/', (ctx, next) => {
  ctx.body = 'Hello Tamriel';
});

router.get('/alive', (ctx, next) => {
  ctx.body = true.toString();
});

router.get('/date', (ctx, next) => {
  let d = moment();
  ctx.body = {
    hour: d.hour(),
    mon: d.month(),
    day: d.date(),
    year: d.year(),
    min: d.minute(),
    sec: d.second()
  };
});

module.exports = router;
