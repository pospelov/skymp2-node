module.exports.randomInteger = function (min, max) {
  var rand = min + Math.random() * (max + 1 - min);
  rand = Math.floor(rand);
  return rand;
};

module.exports.sleep = async function (ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
};

module.exports.clone = function(object) {
  return JSON.parse(JSON.stringify(object));
};

module.exports.getItemTypes = function(type) {
  return ['AMMO', 'ARMO', 'BOOK', 'INGR', 'KEYM', 'MISC', 'ALCH', 'SLGM', 'WEAP', 'SCRL', 'LIGH'];
};
